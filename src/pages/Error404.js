import Banner from '../components/Banner';

export default function Error404() {

    const data = {
        heading: "404 - Page Not Found",
        content: 'The page you are looking for does not exist.',
        destination: '/',
        label: "Go back to homeapge" 
    }


    return (
        <Banner data={data} />
    )
}