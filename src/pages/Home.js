import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import {Button} from 'react-bootstrap';


export default function Home() {

    const data = {
        heading: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere.",
        destination: '/courses',
        label: "Enroll now!"
    }

	return (
		<>
		<Banner data={data} />
    	<Highlights />
		</>
	)
}